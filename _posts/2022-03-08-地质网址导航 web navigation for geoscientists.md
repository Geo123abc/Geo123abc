# 地质网址导航 web navigation for geoscientists.

Share experiences in studying Geology. Sharing is happiness.

## Sci-hub

[sci-hub 总发布页](https://www.sci-hub.pub/)

* [https://sci-hub.ee](https://sci-hub.ee/)
* [https://sci-hub.se](https://sci-hub.se/)
* [https://sci-hub.wf](https://sci-hub.wf/)
* [https://sci-hub.ren](https://sci-hub.ren/)
* [https://sci-hub.shop](https://sci-hub.shop/)
* [https://sci-hub.es.ht](https://sci-hub.es.ht)
* [https://sci-hub.st](https://sci-hub.st)
* [https://sci-hub.it.nf](https://sci-hub.it.nf)

[ResearchGate](https://www.researchgate.net/   "ResearchGate主页")

[X-MOL](https://www.x-mol.com/paper/geo   "X-MOL 地球科学期刊汇总")
